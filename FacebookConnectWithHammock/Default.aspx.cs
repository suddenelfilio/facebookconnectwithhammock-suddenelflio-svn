﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using Hammock;
using System.Collections;
using System.Web.Script.Serialization;


namespace FacebookConnectWithHammock
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["code"]) && !Page.IsPostBack)
            {
                HandleFacebookCallback();
            }
        }

        private void HandleFacebookCallback()
        {
            string CallbackUrl = "http://localhost/FacebookConnectWithHammock/";
            var client = new RestClient { Authority = "https://graph.facebook.com/oauth/" };
            var request = new RestRequest { Path = "access_token" };

            request.AddParameter("client_id", ConfigurationManager.AppSettings["FacebookClientId"]);
            request.AddParameter("redirect_uri", CallbackUrl);
            request.AddParameter("client_secret", ConfigurationManager.AppSettings["FacebookApplicationSecret"]);
            request.AddParameter("code", Request["code"]);

            RestResponse response = client.Request(request);
            // A little helper to parse the querystrings.
            StringDictionary result = ParseQueryString(response.Content);
            string aToken = result["access_token"];

            DisplayUserInformation(aToken);
        }

        private void DisplayUserInformation(string sToken)
        {
            var client = new RestClient { Authority = "https://graph.facebook.com/" };
            var request = new RestRequest { Path = "me" };
            request.AddParameter("access_token", sToken);
            RestResponse response = client.Request(request);

           JavaScriptSerializer ser = new JavaScriptSerializer();
            var parsedResult = ser.Deserialize<FacebookUser>(response.Content);
           
            ProfilePic.ImageUrl = string.Format("http://graph.facebook.com/{0}/picture?type=large",parsedResult.id);
            NameLabel.Text = parsedResult.name;
            AboutLabel.Text = parsedResult.about;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string callbackUrl = "http://localhost/FacebookConnectWithHammock/";
            //Request offline access and publish to the users stream access.
            Response.Redirect(string.Format("https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope=offline_access,publish_stream", ConfigurationManager.AppSettings["FacebookClientId"], callbackUrl));
        }

        internal StringDictionary ParseQueryString(string qstring)
        {
            qstring = qstring + "&";
            var outc = new StringDictionary();
            var r = new Regex(@"(?<name>[^=&]+)=(?<value>[^&]+)&", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            IEnumerator enums = r.Matches(qstring).GetEnumerator();
            while (enums.MoveNext() && enums.Current != null)
            {
                outc.Add(((Match)enums.Current).Result("${name}"),
                         ((Match)enums.Current).Result("${value}"));
            }
            return outc;
        }

    }
}
