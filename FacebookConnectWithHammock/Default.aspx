﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="FacebookConnectWithHammock._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Connect to facebook using Hammock</h2>
    <p>
        Click this button to get connected to your facebook:</p>
    <p>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Connect to facebook" />
    </p>
    <p>
        <asp:Image ID="ProfilePic" runat="server" Height="200px" Width="200px" />
    </p>
    <p>
        Hello
        <strong><asp:Label ID="NameLabel" runat="server" Text="Unknown"></asp:Label></strong>, this is your
        about text that you filled in on facebook:<br />
        <blockquote>
            <asp:Label ID="AboutLabel" runat="server" Text="Unknown"></asp:Label></blockquote>
    </p>
</asp:Content>
