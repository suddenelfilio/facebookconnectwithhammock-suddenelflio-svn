﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacebookConnectWithHammock
{
    public class FacebookUser
    {
        public string   id { get; set; }

        public string name { get; set; }

        public string about { get; set; }

        public string link { get; set; }

    }
}